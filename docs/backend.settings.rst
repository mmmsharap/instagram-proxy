backend.settings package
========================

Submodules
----------

backend.settings.common module
------------------------------

.. automodule:: backend.settings.common
    :members:
    :undoc-members:
    :show-inheritance:

backend.settings.local module
-----------------------------

.. automodule:: backend.settings.local
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: backend.settings
    :members:
    :undoc-members:
    :show-inheritance:
