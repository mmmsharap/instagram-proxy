.. Instagram proxy documentation master file, created by
   sphinx-quickstart on Sun Nov 25 23:22:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Instagram proxy's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2
   
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

