from flask import session


class CurrentUser:
    """Provides info about current user
    """

    key = 'current_user'

    @staticmethod
    def register(user_info):
        """Registrates user info in session

        :param user_info: dictionary with user info
        :type user_info: dict
        :returns: Nothing
        :rtype: None
        """
        session[CurrentUser.key] = user_info

    @staticmethod
    def is_authenticated():
        """Returns `True` if user is authenticated, `False` otherwise
        
        :returns: ``True`` if the value was successfully set, ``False``
            otherwise.
        :rtype: bool
        """
        return CurrentUser.key in session

    @staticmethod
    def get_access_token():
        """Returns access token
        :returns: Access token
        :rtype: str
        """
        return session[CurrentUser.key]['access_token']

    @staticmethod
    def get_info():
        """Returns dictionary with user info

        Example::

            {
                "id": "1574083",
                "username": "snoopdogg",
                "full_name": "Snoop Dogg",
                "profile_picture": "..."
            }

        :returns: Dictionary with user info
        :rtype: dict, None
        """
        if CurrentUser.is_authenticated():
            return session[CurrentUser.key]['user']

    @staticmethod
    def logout():
        """Clears user session

        :returns: Nothing
        :rtype: None
        """
        session[CurrentUser.key] = None
