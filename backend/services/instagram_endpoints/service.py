import requests

from .exceptions import (
    InstagramUnaccessibleError,
    InstagramProvidedErrorError,
)
from backend.services.current_user.service import CurrentUser
from backend.utils.requests import rerases_requests_exception
from backend.constants.misc import (
    INSTAGRAM_API_URL,
    INSTAGRAM_API_GET_RECENT_MEDIA_URL,
)


@rerases_requests_exception(InstagramUnaccessibleError)
def get_recent_media(min_id, max_id, count):
    """Implements `/users/self/media/recent` endpoint request.
    Returns requested data.

    :param min_id: (Optional) Min ID
    :type min_id: int
    :param max_id (Optional) Max ID
    :type max_id int
    :count count: (Optional) Count
    :type count: int

    :raises: `InstagramProvidedErrorError`, Instagram provided error
    :raises: `InstagramUnaccessibleError`, Instagram API is unaccessible

    :returns: Dictionary with recent media
    :rtype: dict
    """
    url = '%s%s' % (INSTAGRAM_API_URL, 
                    INSTAGRAM_API_GET_RECENT_MEDIA_URL)

    response = requests.get(
        url,
        params=dict(
            access_token=CurrentUser.get_access_token(),
            min_id=min_id,
            max_id=max_id,
            count=count,
        )
    ).json()

    if 'error_message' in response.get('meta'):
        raise InstagramProvidedErrorError(
            response['meta']['error_message']
        )

    return response
