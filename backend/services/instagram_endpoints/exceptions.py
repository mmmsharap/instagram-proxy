class InstagramProvidedErrorError(Exception):
    """Instagram provided error
    """
    pass


class InstagramUnaccessibleError(Exception):
    """Instagram API is unaccessible
    """
    pass
