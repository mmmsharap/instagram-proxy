"""
Service implements Instagram authentication after redirecting from Instagram
https://www.instagram.com/developer/authentication/

Example of usage::

    from flask import request

    from backend.services.instagram_authentication.service import process

    try:
        process(request.args)
    except (InstagramProvidedErrorError, InstagramUnaccessibleError,
        InstagramProvidedBadAccessTokenResponseError) as e:
        print(e)
"""
