class InstagramProvidedErrorError(Exception):
    """Instagram provided error
    """
    pass


class InstagramUnaccessibleError(Exception):
    """Instagram API is unaccessible
    """
    pass


class InstagramProvidedBadAccessTokenResponseError(Exception):
    """Instagram provided bad access_token response
    """
    pass
