import requests

from .exceptions import (
    InstagramProvidedErrorError,
    InstagramUnaccessibleError,
    InstagramProvidedBadAccessTokenResponseError,
)
from backend.constants.exception_messages import (
    INSTAGRAM_DIDNT_PROVIDE_CODE,
    INSTAGRAM_PROVIDED_BAD_ACCESS_TOKEN_RESPONSE,
    INSTAGRAM_API_IS_UNACCESSIBLE,
)
from backend.constants.misc import INSTAGRAM_API_GET_ACCES_TOKEN_URL
from backend.utils.requests import rerases_requests_exception
from backend.utils.validation import (
    validate_against_schema,
    ValidationError,
)
from backend.settings import (
    INSTAGRAM_CLIENT_ID,
    INSTAGRAM_CLIENT_SECRET,
    INSTAGRAM_REDIRECT_URI,
)
from backend.services.current_user.service import CurrentUser


def process(args):
    """Processes Instagram answer, raises exception when error is provided.
    Requests the access_token and user info if code is provided then authorizes
    the user and saves his info to user's session object.

    :param args: :class:`werkzeug.datastructures.ImmutableMultiDict` object,
    taken from Flask request
    :type args: werkzeug.datastructures.ImmutableMultiDict

    :raises: `InstagramProvidedErrorError`, Instagram provided error
    :raises: `InstagramUnaccessibleError`, Instagram API is unaccessible
    :raises: `InstagramProvidedBadAccessTokenResponseError`, Instagram provided
        bad access_token response

    :returns: Nothing
    :rtype: None
    """
    if args.get('error'):
        raise InstagramProvidedErrorError(args.get('error_description'))

    if not args.get('code'):
        raise InstagramProvidedErrorError(INSTAGRAM_DIDNT_PROVIDE_CODE)

    instagram_response = _get_instagram_response(args['code'])

    _validate_instagram_response(instagram_response)

    _save_to_session(instagram_response)


@rerases_requests_exception(InstagramUnaccessibleError)
def _get_instagram_response(code):
    """Requests the access_token and user info from Instagram API and returns
    it.

    Required parameters to pass to Instagram API:

        client_id: your client id
        client_secret: your client secret
        grant_type: authorization_code is currently the only supported value
        redirect_uri: the redirect_uri you used in the authorization request.
            Note: this has to be the same value as in the
            authorization request.
        code: the exact code you received during the authorization step

    :param code: code provided by Instagram
    :type code: str
    :rtype: str
    :raises: `InstagramUnaccessibleError`, Instagram API is unaccessible
    """
    response = requests.post(
        INSTAGRAM_API_GET_ACCES_TOKEN_URL,
        data=dict(
            client_id=INSTAGRAM_CLIENT_ID,
            client_secret=INSTAGRAM_CLIENT_SECRET,
            grant_type='authorization_code',
            redirect_uri=INSTAGRAM_REDIRECT_URI,
            code=code,
        )
    )

    return response.json()


def _validate_instagram_response(instagram_response):
    """Validates Instagram response

    :param instagram_response: dictionary with response from Instagram API
    :type instagram_response: dict

    :raises: `InstagramProvidedBadAccessTokenResponseError`, Instagram provided
        bad access_token response

    :returns: Nothing
    :rtype: None
    """
    if (instagram_response is not None and
            instagram_response.get('error_message')):
        # Instagram returns error
        raise InstagramProvidedBadAccessTokenResponseError(
                instagram_response['error_message']
            )

    schema = {
        'access_token': None,
        'user': {
            'id': None,
            'username': None,
            'full_name': None,
            'profile_picture': None,
        },
    }

    try:
        validate_against_schema(instagram_response, schema)
    except ValidationError:
        raise InstagramProvidedBadAccessTokenResponseError(
                INSTAGRAM_PROVIDED_BAD_ACCESS_TOKEN_RESPONSE
            )


def _save_to_session(instagram_response):
    """Saves Instagram response

    :param instagram_response: dictionary with response from Instagram API
    :type instagram_response: dict
    :returns: Nothing
    :rtype: None
    """
    CurrentUser.register(instagram_response)
