import os

from flask import (
    Flask,
    redirect,
    request,
    render_template,
    session,
)

from backend.settings import (
    INSTAGRAM_CLIENT_ID,
    INSTAGRAM_REDIRECT_URI,
)
from backend.constants.assert_messages import (
    INSTAGRAM_CLIENT_ID_IS_NOT_SPECIFIED_MESSAGE,
)
from backend.utils.session import RedisSessionInterface
from backend.utils.redis import redis
from backend.services.current_user.service import CurrentUser
from backend.services.instagram_authentication.service import (
    process as process_instagram_authentication,
)
from backend.services.instagram_authentication.exceptions import (
    InstagramProvidedErrorError,
    InstagramUnaccessibleError,
    InstagramProvidedBadAccessTokenResponseError,
)
from backend.api.instagram.v1 import instagram_api as instagram_api_v1
from backend.services.instagram_endpoints.service import (
    get_recent_media,
)
from backend.services.instagram_endpoints.exceptions import (
    InstagramProvidedErrorError,
    InstagramUnaccessibleError,
)
from backend.constants.misc import DEFAULT_RECENT_MEDIA_COUNT
from backend.api.instagram.v1.adapters import recent_media_to_dict_adapter


assert INSTAGRAM_CLIENT_ID is not None, INSTAGRAM_CLIENT_ID_IS_NOT_SPECIFIED_MESSAGE  # noqa


app = Flask(
    __name__,
    template_folder=os.path.abspath('backend/templates'),
)

app.register_blueprint(instagram_api_v1)

app.session_interface = RedisSessionInterface(redis=redis,
                                              prefix="flask-session:")


@app.route('/')
def index():
    """Returns index page
    """
    is_authenticated = CurrentUser.is_authenticated()
    posts = []
    
    if is_authenticated:
        try:
            recent_media = get_recent_media(
                    request.args.get('min_id'),
                    request.args.get('max_id'),
                    request.args.get('count', 
                                     DEFAULT_RECENT_MEDIA_COUNT),
                )
        except (InstagramUnaccessibleError, 
                InstagramProvidedErrorError) as e:
            return render_template('error.html', error=str(e))

        posts = recent_media_to_dict_adapter(recent_media)['objects'];
        max_id = None
        min_id = None
        
        if posts:
            max_id = max([post['id'] for post in posts])
            min_id = min([post['id'] for post in posts])
    
    return render_template('index.html',
                           is_authenticated=is_authenticated,
                           user=CurrentUser.get_info(),
                           posts=posts,
                           max_id=max_id,
                           min_id=min_id)


@app.route('/login')
def login():
    """Redirects to Instagram authorization url
    """
    instagram_authorization_url = (
        'https://api.instagram.com/oauth/authorize/?response_type=code'
        f'&client_id={INSTAGRAM_CLIENT_ID}'
        f'&redirect_uri={INSTAGRAM_REDIRECT_URI}'
    )

    return redirect(instagram_authorization_url)


@app.route('/authorize/instagram')
def authorize_instagram():
    """Receives the redirect from Instagram.
    Processes the answer, save access_token to session
    """
    try:
        process_instagram_authentication(request.args)
    except (InstagramProvidedErrorError, InstagramUnaccessibleError,
            InstagramProvidedBadAccessTokenResponseError) as e:
        return render_template('error.html', error=str(e))

    return redirect('/')


@app.route('/logout')
def logout():
    """Log out
    """
    session.clear()
    return redirect('/')
