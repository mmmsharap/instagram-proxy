def recent_media_to_dict_adapter(recent_media):
    """Adapts recent media data from Instagram API to our API 
    (instagram.v1.api.GetRecentMediaAPI)

    :param recent_media: Dictionary with recent media
    :type recent_media: dict

    :returns: Adapted dictionary
    :rtype: dict
    """
    result = dict(objects=[])

    for entry in recent_media.get('data'):
        adapted_entry = dict()

        adapted_entry['id'] = entry['id']
        adapted_entry['type'] = entry['type']
        adapted_entry['images'] = entry['images']

        if entry.get('caption'):
            adapted_entry['title'] = entry['caption']['text']

        if 'videos' in entry:
            adapted_entry['videos'] = entry['videos']

        result['objects'].append(adapted_entry)

    return result
