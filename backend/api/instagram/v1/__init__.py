from flask import Blueprint

from .api import GetRecentMediaAPI
from .constants import ROOT_URL


instagram_api = Blueprint('instagram', __name__)

instagram_api.add_url_rule('%s%s' % (ROOT_URL, 'get_recent_media'), view_func=GetRecentMediaAPI.as_view('get_recent_media'))

__all__ = [
    'instagram_api'
]
