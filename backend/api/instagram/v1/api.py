from flask import request

from backend.utils.api import APIEndpoint
from backend.services.instagram_endpoints.service import (
    get_recent_media,
)
from backend.services.instagram_endpoints.exceptions import (
    InstagramProvidedErrorError,
    InstagramUnaccessibleError,
)
from backend.constants.misc import DEFAULT_RECENT_MEDIA_COUNT
from .adapters import recent_media_to_dict_adapter


class GetRecentMediaAPI(APIEndpoint):

    def get(self):
        """Gets recent media from Instagram API and returns it.
        If something wrong with Instagram API, returns 500 status code.

        :param request.args.get('min_id'): (Optional) Min ID
        :type request.args.get('min_id'): int
        :param request.args.get('max_id'): (Optional) Max ID
        :type request.args.get('max_id'): int

        Response example:

            {
                "objects": [
                    {
                        "id": ...,
                        "type": ...,
                        "images": {
                            "low_resolution": {
                                "url": ...,
                                "width": ...,
                                "height": ...
                            },
                            "thumbnail": {
                                "url": ...,
                                "width": ...,
                                "height": ...
                            },
                            "standard_resolution": {
                                "url": ...,
                                "width": ...,
                                "height": ...
                            }
                        },
                        // Optional
                        "title": ...,
                        "videos": {
                            "low_resolution": {
                                "url": ...,
                                "width": ...,
                                "height": ...
                            },
                            "standard_resolution": {
                                "url": ...,
                                "width": ...,
                                "height": ...
                            },
                        },
                    },
                    ...
                ]
            }

        Error response example:
            
            {
                "error": "Instagram API is unaccessible"
            }

        :returns: Dictionary with recent media
        :rtype: dict
        """
        try:
            recent_media = get_recent_media(
                    request.args.get('min_id'),
                    request.args.get('max_id'),
                    request.args.get('count', 
                                     DEFAULT_RECENT_MEDIA_COUNT),
                )
        except (InstagramUnaccessibleError, 
                InstagramProvidedErrorError) as e:
            return { 'error': str(e) }, 500

        result = recent_media_to_dict_adapter(recent_media)

        return result


