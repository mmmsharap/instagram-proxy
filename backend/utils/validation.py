def validate_against_schema(dictionary, schema):
    """Validates dictionary against given schema recursively

    :param dictionary: given dictionary
    :type dictionary: dict
    :param schema: recursive dictionary where value can be either a `None` and
        it means dictionary should have by that key or other
        schema dictionary
    :type schema: dict
    :raises: `ValidationError`, Validation error
    :returns: Nothing
    :rtype: None
    """
    for key, value in schema.items():
        try:
            if isinstance(value, dict):
                validate_against_schema(dictionary[key],  value)
            else:
                # Allow some non-dict value but ignore it
                dictionary[key]
        except TypeError as e:
            raise ValidationError


class ValidationError(Exception):
    """Validation error
    """
    pass
