from functools import wraps
from mimerender import FlaskMimeRender
from flask import (
    current_app,
    json,
    jsonify as _jsonify,
    request,
    abort,
)
from flask.views import MethodView

from backend.services.current_user.service import CurrentUser
from backend.constants.http_status_codes import FORBIDDEN_STATUS_CODE


#: String used internally as a dictionary key for passing header information
#: from view functions to the :func:`jsonpify` function.
_HEADERS = '__headers'

#: String used internally as a dictionary key for passing status code
#: information from view functions to the :func:`jsonpify` function.
_STATUS = '__status_code'


def jsonify(*args, **kw):
    """Same as :func:`flask.jsonify`, but sets response headers.
    If ``headers`` is a keyword argument, this function will construct the JSON
    response via :func:`flask.jsonify`, then set the specified ``headers`` on
    the response. ``headers`` must be a dictionary mapping strings to strings.
    """
    response = _jsonify(*args, **kw)
    if 'headers' in kw:
        set_headers(response, kw['headers'])
    return response


def jsonpify(*args, **kw):
    """Passes the specified arguments directly to :func:`jsonify` with a status
    code of 200, then wraps the response with the name of a JSON-P callback
    function specified as a query parameter called ``'callback'`` (or does
    nothing if no such callback function is specified in the request).
    If the keyword arguments include the string specified by :data:`_HEADERS`,
    its value must be a dictionary specifying headers to set before sending the
    JSONified response to the client. Headers on the response will be
    overwritten by headers specified in this dictionary.
    If the keyword arguments include the string specified by :data:`_STATUS`,
    its value must be an integer representing the status code of the response.
    Otherwise, the status code of the response will be :http:status:`200`.
    """
    # HACK In order to make the headers and status code available in the
    # content of the response, we need to send it from the view function to
    # this jsonpify function via its keyword arguments. This is a limitation of
    # the mimerender library: it has no way of making the headers and status
    # code known to the rendering functions.
    headers = kw.pop(_HEADERS, {})
    status_code = kw.pop(_STATUS, 200)
    response = jsonify(*args, **kw)
    callback = request.args.get('callback', False)
    if callback:
        # Reload the data from the constructed JSON string so we can wrap it in
        # a JSONP function.
        data = json.loads(response.data)
        # Force the 'Content-Type' header to be 'application/javascript'.
        #
        # Note that this is different from the mimetype used in Flask for JSON
        # responses; Flask uses 'application/json'. We use
        # 'application/javascript' because a JSONP response is valid
        # Javascript, but not valid JSON.
        headers['Content-Type'] = 'application/javascript'
        # Add the headers and status code as metadata to the JSONP response.
        meta = _headers_to_json(headers) if headers is not None else {}
        meta['status'] = status_code
        inner = json.dumps(dict(meta=meta, data=data))
        content = '{0}({1})'.format(callback, inner)
        # Note that this is different from the mimetype used in Flask for JSON
        # responses; Flask uses 'application/json'. We use
        # 'application/javascript' because a JSONP response is not valid JSON.
        mimetype = 'application/javascript'
        response = current_app.response_class(content, mimetype=mimetype)
    # Set the headers on the HTTP response as well.
    if headers:
        set_headers(response, headers)
    response.status_code = status_code
    return response


json_mimerender = FlaskMimeRender()(default='json', json=jsonpify)


def login_required(func):
    """Decorator that closes method from Anonimous users

    :param func: function to decorate
    :type func: callable
    :returns: Wrapped function
    :rtype: Callable
    """
    @wraps(func)
    def wrapped(*args, **kwargs):
        """Closes method from Anonimous users
        """
        if not CurrentUser.is_authenticated():
            return abort(FORBIDDEN_STATUS_CODE)

        return func(*args, **kwargs)

    return wrapped
        

class APIEndpoint(MethodView):
    """Base closed API class
    """

    decorators = [json_mimerender, login_required]


class PublicAPIEndpoint(MethodView):
    """Base public API class
    """

    decorators = [json_mimerender]
