import redis as _redis

from urllib.parse import urlparse

from backend.settings import REDIS_URL


def get_redis(url):
    '''Returns redis client instance

    :param url: redis url. example: `redis://localhost/0`
    :type url: str
    :rtype: redis.StrictRedis
    '''
    url = urlparse(url)
    host, port = url.hostname, url.port
    db = url.path.split('/')[1]

    redis_kwargs = dict(
        host=host,
        db=db,
    )

    if port:
        redis_kwargs['port'] = port

    return _redis.StrictRedis(**redis_kwargs)


redis = get_redis(REDIS_URL)
