import socket
from functools import wraps

from requests.exceptions import RequestException


def rerases_requests_exception(by_exception):
    """Decorator that rerases `requests` exceptions by given exception

    :param by_exception: exception to reraze
    :type by_exception: subclass of `Exception`
    :returns: Decorator
    :rtype: Callable
    """
    def decorator(func):
        """
        :param func: function to decorate
        :type func: callable
        :returns: Wrapped function
        :rtype: Callable
        """
        @wraps(func)
        def wrapped(*args, **kwargs):
            """Rerases `requests` exceptions by given exception
            """
            
            try:
                return func(*args, **kwargs)
            except (RequestException, socket.timeout) as e:
                # We need also handle socket.timeout exception here because of
                # urllib3 bug https://github.com/requests/requests/issues/1236
                by_exception(str(e))

        return wrapped

    return decorator
