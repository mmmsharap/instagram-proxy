import os
from .common import *

if os.environ.get('TESTING', None):
    from .tests import *
else:
    try:
        from .local import *
    except ImportError:
        pass
