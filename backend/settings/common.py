"""
Common settings can be adjusted by settings from `./test.py` or `./local.py`
depending on enviroment.
"""

INSTAGRAM_CLIENT_ID = None
INSTAGRAM_CLIENT_SECRET = None
INSTAGRAM_REDIRECT_URI = 'http://127.0.0.1:5000/authorize/instagram'

REDIS_URL = 'redis://redis/0'
