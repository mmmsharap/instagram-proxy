INSTAGRAM_DIDNT_PROVIDE_CODE = "Instagram didn't provide code"
INSTAGRAM_API_IS_UNACCESSIBLE = "Instagram API is unaccessible"
INSTAGRAM_PROVIDED_BAD_ACCESS_TOKEN_RESPONSE = (
    "Instagram provided bad access_token response"
)
