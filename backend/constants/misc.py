INSTAGRAM_API_GET_ACCES_TOKEN_URL = (
    "https://api.instagram.com/oauth/access_token"
)
INSTAGRAM_API_URL = "https://api.instagram.com/v1"
INSTAGRAM_API_GET_RECENT_MEDIA_URL = "/users/self/media/recent"
DEFAULT_RECENT_MEDIA_COUNT = 10
