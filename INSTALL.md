## Requirements

- docker
- docker-compose
- Create backend/settings/local.py file and fill INSTAGRAM_CLIENT_ID and
INSTAGRAM_CLIENT_SECRET constants

## Run
    
    docker-compose build
    docker-compose up

By default service will be accessed at `http://127.0.0.1:5000/`

## Docs

    user> docker exec -it instagram-proxy-backend bash
    root@XXXXXXX:/app/# cd docs
    root@XXXXXXX:/app/docs# make build

Then open docs/_build/index.html
